package main

import (
	"fmt"
	"net/http"
	"os"
	"strings"

	"bitbucket.org/selman/things/thing"
	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.POST("/md", mdHandler)
	router.Run(":" + os.Getenv("PORT"))
}

func mdHandler(c *gin.Context) {
	payload := make(thing.Things)
	c.Bind(&payload)
	ss := s(1, payload)
	c.String(http.StatusOK, ss)
}

func s(i int, t thing.Things) string {
	var result string
	for key, val := range t {
		header := strings.Repeat("#", i)
		quote := strings.Repeat(">", i)
		result += fmt.Sprintf("%s %s  %s\n", quote, header, key)
		result += fmt.Sprintf("%s  %s\n", quote, val.Note)
		if sub := val.SubThings; len(sub) != 0 {
			result += s(i+1, sub)
		}

	}

	return result
}
