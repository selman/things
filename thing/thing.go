package thing

import (
	"errors"
	"strings"
	//"fmt"
)

type Thing struct {
	Note      string
	SubThings Things
}

type Things map[string]*Thing

func (t *Thing) Add(title string, thing Thing) error {
	if len(t.SubThings) == 0 {
		t.SubThings = make(Things)
	}

	err := t.SubThings.Add(title, thing)
	if err != nil {
		return err
	}
	return nil
}

func (ts Things) Add(title string, thing Thing) error {

	if _, ok := ts[title]; ok {
		return errors.New("Key already added: " + title)
	}

	ts[title] = &thing
	return nil
}

func (ts Things) AddTo(path string, title string, thing Thing) error {
	if len(path) == 0 {
		return errors.New("Empty path given")
	}

	elems := strings.Split(path, ".")
	t := ts.walk(elems)
	err := t.Add(title, thing)
	if err != nil {
		return err
	}

	return nil
}

func (ts Things) walk(p []string) *Thing {
	if len(p) != 1 {
		x := ts[p[0]].SubThings
		t := x.walk(p[1:])
		return t
	} else {
		//ts[p[0]] = new(Thing)
		x := ts[p[0]]
		return x
	}

}
